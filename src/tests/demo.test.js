

describe('Pruebas en el archivo demo.test.js', () => {
  test( 'deben de ser iguales los string', () => {
      // 1. Arrange
      const mensaje = 'Hola Mundo';

      // 2. act
      const mensaje2 = `Hola Mundo`;

      // 3. Assert
      expect( mensaje ).toBe( mensaje2 ); // ===
  })
});


